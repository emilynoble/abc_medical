const toggleButton = document.getElementsByClassName('togglebtn')[0]
const navbarLinks = document.getElementsByClassName('nav_links')[0]

toggleButton.addEventListener('click', () => {
    navbarLinks.classList.toggle('active')
})

function submitButton() {
    var button = document.getElementById("submit");
    button.style.backgroundColor = "black";
    button.style.color = "white";
    button.style.border = "none";

    var form = document.getElementById("form");
    var thanks = document.getElementById("thanks");

    form.style.display = "none";
    thanks.style.display = "block";
}
  